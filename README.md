# Energy Trading on the Smart Grid (Ethereum/Solidity Component)


* The docker folder contains scripts to help setup a local docker network of nodes. It is currently unused and can safely be ignored.

* The local is where my current network code is. It contains the python command network as well as helper files for compiling smart contracts and interacting with ethereum nodes.

* The solidity folder contains the smart contract and its compiled files. 

Check each subfolders readme for more details.