Ethereum Private Network Playground 
==========

# Issues with docker networking forced me away from a docker implementation so now this folder is unused. Feel free to look around!

This works is based on the official "ethereum/go-client" docker image, without modifications. A few scripts in this project simplify playing with this image.

# Naming Convention

Scripts starting with new (newRunnode newBootnode etc..) are for use when setting up a private network with multiple different machines. Scripts without new 
are for use with a local network. 

## Quick start (local network)

Assuming you have Docker up and running:

* Run a bootnode
* Run 3 common nodes
* Set node1 to mine
* Load a helper script to each node

```sh
./setup.sh
```

## Quick Start (different machines)

Coming soon s