#!/bin/sh

#Compile solidity contract
cd ../solidity

solc -o . --bin --abi --overwrite energyTrading.sol

#Copy compiled into js file.
abi=`cat EnergyTrader.abi`
cd ../docker
sed -i.bak "s/var energy.*/var energy = eth.contract(${abi});/g" contract.js

#bin is still manual. 

cd ../solidity
bin=`cat EnergyTrader.bin`
binLen=${#bin}

half=$((binLen / 2))

shalf=$((half - 1))

fh=`head -c ${shalf} EnergyTrader.bin`
lh=`tail -c+${half} EnergyTrader.bin`

#rm t.txt
#echo ${fh} > t.txt
#echo ${lh} >> t.txt
cd ../docker
sed -i.bak "s/var bc = .*/var bc = \"0x${fh}\"/" contract.js
sed -i.bak "s/bc = bc +.*/bc = bc + \"${lh}\"/" contract.js


docker cp contract.js ethereum-node1:/opt/c.js
docker cp contract.js ethereum-node2:/opt/c.js
docker cp contract.js ethereum-node3:/opt/c.js