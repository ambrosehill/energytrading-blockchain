docker stop ethereum-bootnode
docker rm ethereum-bootnode

mkdir -p $(pwd)/.bootnode

docker run --rm -v $(pwd)/.bootnode:/opt/bootnode "ethereum/client-go:alltools-v1.7.3" bootnode --genkey /opt/bootnode/boot.key

docker run -d --name ethereum-bootnode -v $(pwd)/.bootnode:/opt/bootnode -p 30301:30301 "ethereum/client-go:alltools-v1.7.3" bootnode --nodekey /opt/bootnode/boot.key --verbosity=3 "$@"
