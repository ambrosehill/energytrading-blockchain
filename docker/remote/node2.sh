#!/bin/bash
IMGNAME="ethereum/client-go:v1.7.3"
NODE_NAME=$1
NODE_NAME=${NODE_NAME:-"node2"}
CONTAINER_NAME="ethereum-$NODE_NAME"
DATA_ROOT=${DATA_ROOT:-"$(pwd)/.ether-$NODE_NAME"}
echo "Destroying old container $CONTAINER_NAME..."
docker stop $CONTAINER_NAME
docker rm $CONTAINER_NAME
RPC_PORTMAP=

RPC_ARG='--rpc --rpcaddr=0.0.0.0 --rpcapi=db,eth,net,web3,personal --rpccorsdomain "*"'
RPC_PORTMAP="-p 8545:8545"
BOOTNODE_URL=enode://f317f2cc28c5fe306871673bcb3658135ec29bd9a7b34983bfc79798d8dc9d205311bd524c2cda3950d51a9ec1049a0472c5e5ca0bba0dcda7666d3081afab5f@172.17.0.2:30301


if [ ! -f $(pwd)/genesis.json ]; then
    echo "No genesis.json file found, please run 'genesis.sh'. Aborting."
    exit
fi
if [ ! -d $DATA_ROOT/keystore ]; then
    echo "$DATA_ROOT/keystore not found, running 'geth init'..."
    docker run --rm \
        -v $DATA_ROOT:/root/.ethereum \
        -v $(pwd)/genesis.json:/opt/genesis.json \
        $IMGNAME init /opt/genesis.json
    echo "...done!"
fi
echo "Running new container $CONTAINER_NAME..."
docker run -d --name $CONTAINER_NAME \
           -p 30302:30302 \
           -v $DATA_ROOT:/root/.ethereum \
           -v $(pwd)/genesis.json:/opt/genesis.json \
           $IMGNAME --bootnodes=$BOOTNODE_URL $RPC_ARG  --port 30302 --cache=512 --verbosity=4 --maxpeers=3 ${@:2}