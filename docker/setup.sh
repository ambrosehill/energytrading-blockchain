#!/bin/bash

#Script to set up docker environment


#startbootnode

./bootnode.sh


#start 3 normal nodes

./runnode.sh node1
./runnode.sh node2
./runnode.sh node3

#Send helper script to each node
docker cp contract.js ethereum-node1:/opt/c.js
docker cp contract.js ethereum-node2:/opt/c.js
docker cp contract.js ethereum-node3:/opt/c.js

#copy private key to each nodes keystore. 
cp pk-2 .ether-node2/keystore
cp pk-3 .ether-node3/keystore


#Start node 1 mining
docker exec -ti "ethereum-node1" geth --exec 'personal.newAccount("test")' attach
docker exec -ti "ethereum-node1" geth --exec 'miner.start(1)' attach
