Local Machine Test Network
==========================

This folder contains the command network code as well as helper files for working with and developing on blockchain, specifically ethereum and solidity.

Files
=====

+ benchmark.py - Used to benchmark the energy trading network speed. It times how fast a CTP, ERC and complete trade can be computed by the network.
+ clear.sh - Used to delete the current backlog in the test folders eth1, eth2 and eth3. It also calls geth init on these folders using genesis.json.
+ compileContract.sh - Used to compile the contract in the solidity folder as well as copy the .bin contents into helper.js.
+ cost_benchmarking.py - Used to benchmark the costs if each transaction a participating node will make.
+ db_interface.py - Used as a manual entry point to the nodes database. Allows for easy testing of network.
+ genesis.json - Used in the geth init command to initlise all nodes with the same network id and pre allocate funds to the private keys stored in pk-1/2/3.
+ helper.js - A helper script that can be loaded into an ethereum nodes javascript console. Has a bunch of shortcuts that makes interacting with ethereum nodes easier.
+ miner_database.py - The miner nodes database mangemant code. Pony ORM was used so all database entries can be accessed through python classes.
+ network.py - Main network script that connects to an ethereum node and forces that node to react to events emitted from the smart contract.
+ node_database.py - The nodes database management code. Pony ORM was used so all database entries can be accessed through python classes.
+ pi_network.py - A simplified version of network.py to run on raspberry pis. The main difference is the hashing is removed and replaced with a already decrypted public key.
+ pk-1/2/3 - Used to pre allocate funds to ethereum nodes in test networks.
+ requirements.txt - python3 modules.
+ time_benchmarking.py - An improved version of benchmarking.py


Setup
=====

1. Initlise the ethereum node folders with genesis.json
2. Start each geth node with a command similiar to ** geth --datadir dir --rpc --rpcapi="db,eth,net,web3,personal" --networkid n-ID console **
3. Load helper.js into each node using **loadScript('path-to-helper.js')**
4. Manually grab the enode of each node using **'enode'** in the javascript console. Copy + paste that enode into every other nodes console using **admin.addPeer(enode)**
5. Now the ethereum network is setup all that is left is to connect the command network.
6. The miner node can be setup using **'python3 network.py miner'**.
7. If the other nodes are on the same machine than the **--rpcport port** flag must be used to start other nodes. This port must then be added to the node operator section in network.py. **'python3 network.py node'**
8. Once the miner and 1 peer have python nodes running, use **'python3 db_interface.py'** to interact with the nodes database and input CTP's and ERC's. These will be picked up automatically and propagated around the network.

### Standard Trade Process
1. Node receives CTP, sends it to smart contract.
2. Node sends signed transaction to miner node.
3. Node receives energy and sends ERC to smart contract.
4. Miner is notified of completed trade and mines corresponding signed transaction from step 2.