import node_database as nd
import time
import web3
import datetime
import random

#Connect to node
w3 = web3.Web3(web3.Web3.HTTPProvider('http://localhost:8885'))

#Connect to database
nd.db.generate_mapping(create_tables=True)

#Address variables
eth2 = "0xa87c00750babf601c2b8d21ff3ed85544d75ed32"
eth3 = "0x1147494773a0769c652ec0404a654f46022a5ad4"
eth4 = "0xb4488785290dd8ec8dbf300f4c65a608c3a8b153"

to_ad = "0x1283afc3b179fd7004d4fd66ad566803ff15966c"
from_ad = w3.eth.coinbase

#Amount to send
amount = 1
#Number of transactions
nTx = 10
#Benchmark number
benchmark = 1

#Access the contract
#Contract variables
with open('../solidity/EnergyTrader.abi') as f:
    ABI = f.read().strip()

address = "0xb600289ebe84ee54c1d3e8aa30365ac60d6cda59"

#Create contract instance to interact with
contract = w3.eth.contract(
    address=w3.toChecksumAddress(address),
    abi=ABI
)

start = w3.eth.getBlock('latest')['number']

#Access the events from the smart contract
ctp = contract.eventFilter('ctp_accepted',{'fromBlock': start,'toBlock':'latest'})



#Load in 100 test cases
print('Loading db')
for x in range(0,nTx):
    nd.create_ctp(to_ad=to_ad, from_ad=from_ad, amount=amount)
    amount += 100

#capture start time
startTime = datetime.datetime.now()
print('Entering While loop')

while True:
    events = ctp.get_new_entries()
    for event in events:
        
        from_ad = event['args']['from']
        to_ad = event['args']['to']
        amount = event['args']['amount']
        ctp_id = event['args']['ctp_id']
        print('ctp_accepted found: ',ctp_id)
        if from_ad == w3.eth.coinbase:
            ran = random.choice(range(0,101))
            if ran > 102:
                print('Skipping as ran is: ', ran)
                continue
                print('###################')
            time.sleep(2)
            print('Creating erc with id: ',ctp_id)
            nd.create_erc(ctp_id)
        print('############################')
    if nd.get_ctps() is None:
        break

print('{} Txs sent in {} seconds'.format(nTx,datetime.datetime.now() - startTime))