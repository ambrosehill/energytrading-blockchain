#!/bin/sh

#Remove node directories
rm -rf eth1/*
rm -rf eth2/*
rm -rf eth3/*

#remove databases
rm *.sqlite

#Reset directories
~/Desktop/dockerTest/go-ethereum/build/bin/geth --datadir eth1 init genesis.json
~/Desktop/dockerTest/go-ethereum/build/bin/geth --datadir eth3 init genesis.json
~/Desktop/dockerTest/go-ethereum/build/bin/geth --datadir eth2 init genesis.json

#Copy private keys to nodes
cp pk-2 eth2/keystore/pk
cp pk-3 eth3/keystore/pk
