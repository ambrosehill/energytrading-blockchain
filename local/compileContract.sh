#!/bin/sh

#Compile solidity contract
cd ../solidity
solc -o . --bin --abi --overwrite energyTrading.sol

#Setup .bin object, since its so big, split it in half
bin=`cat EnergyTrader.bin`

#Split the length into two parts
binLen=${#bin}
half=$((binLen / 2))
shalf=$((half - 1))
#Get the first and last half of the .bin file into varibles
fh=`head -c ${shalf} EnergyTrader.bin`
lh=`tail -c+${half} EnergyTrader.bin`

#Get the abi into a variable
abi=`cat EnergyTrader.abi`

#Go back to local setup directory
cd ../local
#Copy the abi into the contract helper file for easy deployment
sed -i '' "s/var energy.*/var energy = eth.contract(${abi});/g" helper.js

#Copy the bin into the contract helper file for easy deployment
sed -i '' "s/var bc = .*/var bc = \"0x${fh}\"/" helper.js
sed -i '' "s/bc = bc +.*/bc = bc + \"${lh}\"/" helper.js