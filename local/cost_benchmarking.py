import web3
import node_database as nd
import time
import datetime
import random




#Have inital balance. 
#send 100 transactions to SC
#Check cost of each
#average them

w3 = web3.Web3(web3.Web3.HTTPProvider('http://localhost:8885'))
to_ad = "0x1147494773a0769c652ec0404a654f46022a5ad4"

with open('../solidity/EnergyTrader.abi') as f:
    ABI = f.read().strip()



address = "0x441f7d8e95d682d7249675140da1ed0e5c014d1b"

#Create contract instance to interact with
contract = w3.eth.contract(
    address=w3.toChecksumAddress(address),
    abi=ABI
)

#Access the events from the smart contract
ctp = contract.eventFilter('ctp_accepted',{'fromBlock': 0,'toBlock':'latest'})
#deploy = contract.eventFilter('deploy',{'fromBlock': 0, 'toBlock': 'latest'})
trade = contract.eventFilter('trade',{'fromBlock': 0,'toBlock':'latest'})


def getBal():
    return w3.fromWei(w3.eth.getBalance(w3.eth.coinbase),"ether")


def cost_ctp(i):
    currBal = getBal()
    valid = w3.toChecksumAddress(to_ad)
    transaction = {
        'from': w3.eth.coinbase
    }
    w3.personal.unlockAccount(w3.eth.coinbase,'test')
    contract.functions.acceptCTP(i,int(i),valid).transact(transaction)
    print('waiting for event')
    while True:
        events = ctp.get_new_entries()
        for event in events:
            print('event found')
            newBal = currBal - getBal()
            return newBal

def cost_erc(i):
    currBal = getBal()
    valid = w3.toChecksumAddress(to_ad)
    transaction = {
        'from': w3.eth.coinbase
    }
    w3.personal.unlockAccount(w3.eth.coinbase,'test')
    contract.functions.receiveERC(i).transact(transaction)
    print('waiting for event')
    while True:
        events = trade.get_new_entries()
        for event in events:
            print('event found')
            newBal = currBal - getBal()
            return newBal


i = 0
ctp_costs = []
erc_costs = []
while i < 101:
    ctp_costs.append(cost_ctp(i))
    erc_costs.append(cost_erc(i))
    i += 1

ctpAverage = 0
for c in ctp_costs:
    print('ctp took: ',c)
    ctpAverage += c

ercAverage = 0
for e in erc_costs:
    print('erc took: ',e)
    ercAverage += e



print('average cost for 100 CTP: ', ctpAverage/100)
print('average cost for 100 ERC: ', ercAverage/100)