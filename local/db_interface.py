from node_database import db,create_ctp,create_erc,get_ctps,db_session

db.generate_mapping(create_tables=True)
eth2ad = "0xa87c00750babf601c2b8d21ff3ed85544d75ed32"
eth3ad = "0x1147494773a0769c652ec0404a654f46022a5ad4"
eth4ad = "0xb4488785290dd8ec8dbf300f4c65a608c3a8b153"

def welcome():
	print('========================================')
	print('Welcome to the Energy Trading interface')
	print('Please enter option: ')
	print('** ctp to amount')
	print('** erc ctp_id')
	print('========================================')


from_ad = eth2ad

welcome()
while True:

	user = input()
	#print(user)
	x = user.rsplit(' ')
	#print(x)
	with db_session:
		if x[0] == 'ctp' or x[0] == 'CTP':
			if len(x) != 3:
				print('Error: Incorrect Number of Arguments')
			else:
				to_ad = x[1]
				amount = x[2]
				if to_ad == "eth2":
					to_ad = eth2ad
				if to_ad == "eth3":
					to_ad = eth3ad
				if to_ad == "eth4":
					to_ad = eth4ad

				print('#############################################################################')
				print('Creating CTP \n To Address: {to} \n From Address: {fr} \n Amount: {a}'.format(to=to_ad,fr=from_ad,a=amount))
				print('#############################################################################')
				create_ctp(to_ad=to_ad, from_ad=from_ad, amount=amount)
		elif x[0] == 'erc' or x[0] == 'ERC':
			if len(x) != 2:
				print('Error: incorrect number of arguments')
			else:
				ctp_id = x[1]
				print('########################')
				print('Creating ERC \n ID: ', ctp_id)
				print('########################')
				create_erc(ctp_id)

		else:
			print('#############################')
			print('# Please Enter Valid Option #')
			print('#############################')
			welcome()