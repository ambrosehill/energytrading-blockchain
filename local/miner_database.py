from pony.orm import Database, Required, Optional, Set, db_session, select

########################################################################
#	MINERS Database 												   #
########################################################################
#Database file for energy trading thesis
#Create and connect to database.
db = Database()
db.bind(provider='sqlite', filename='miner.sqlite', create_db=True)

#Classes
class M_ctp(db.Entity):
	ctp_id = Required(int)
	from_ad = Required(str)
	raw = Required(str)

class Miner_CTP(db.Entity):
	ctp_hash = Required(str)
	ctp_id = Required(int)
	from_ad = Required(str)
	raw = Required(str)

@db_session
def create_raw(ctp_id,from_ad,raw):
	return M_ctp(ctp_id=ctp_id,from_ad=from_ad,raw=raw)

@db_session
def delete_raw(ctp_id,from_ad):
	#print('DELETING RAW WITH ID: ',ctp_id)
	m = M_ctp.get(ctp_id=ctp_id,from_ad=from_ad)
	if m is not None:
		m.delete()
		return True
	return False

@db_session
def get_raw(ctp_id,from_ad):
	q = M_ctp.get(ctp_id=ctp_id,from_ad=from_ad)
	#print('Looking for raw with id {} and ad {}'.format(ctp_id,from_ad))
	#print_out_db()
	if q is None:
		return None
	return q

@db_session
def print_out_db():
	print('database contains: ')
	query = M_ctp.select()
	for q in query:
		print('Id: {} Address: {}'.format(q.ctp_id,q.from_ad))



@db_session
def create_raw1(sha256,ctp_id,from_ad,raw):
	return Miner_CTP(ctp_hash=sha256,ctp_id=ctp_id,from_ad=from_ad,raw=raw)

@db_session
def delete_raw1(sha256):
	q = Miner_CTP.get(ctp_hash=sha256)
	if q is not None:
		#print('DELETING RAW WITH ID: ',q.ctp_id)
		q.delete()
		return True
	return False

@db_session
def get_raw1(sha256):
	q = Miner_CTP.get(ctp_hash=sha256)
	if q is None:
		return None
	return q
