from pony.orm import Database, Required, Optional, Set, db_session, select


########################################################################
#	Nodes Database 												   	   #
########################################################################
#Database file for energy trading thesis
#Create and connect to database.
db = Database()
db.bind(provider='sqlite', filename='eth.sqlite', create_db=True)

#Classes

class CTP(db.Entity):
	from_ad = Required(str)
	to_ad = Required(str)
	amount = Required(int)

class ERC(db.Entity):
	ctp_id = Required(int,unique=True)



#Helper Functions

#CTP
@db_session
def create_ctp(to_ad, from_ad, amount):
	return CTP(to_ad=to_ad,from_ad=from_ad,amount=amount)

@db_session
def get_ctp(amount,from_ad,to_ad):
	return CTP.get(amount=amount,from_ad=from_ad,to_ad=to_ad)


@db_session
def delete_ctp(amount,from_ad,to_ad):
	ctp = get_ctp(amount,from_ad,to_ad)
	ctp.delete()

@db_session
def delete_erc(ctp_id):
	erc = get_erc_id(ctp_id)
	erc.delete()

@db_session
def get_erc_id(ctp_id):
	return ERC.get(ctp_id=ctp_id)

@db_session
def create_erc(new_id):
	return ERC(ctp_id=new_id)

@db_session
def get_ctps():
	return CTP.select().first()

@db_session
def get_ercs():
	return ERC.select().limit(1)


