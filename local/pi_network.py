import web3
import time
import sys
import random
import node_database
import hashlib
import socket
import threading

#===============#
#SETUP VARIABLES#
#===============#
#Is this a miner or a node?
w3 = web3.Web3(web3.Web3.HTTPProvider('http://localhost:8545'))
operator = "node"
#Welcome message
print('Welcome to Energy Trader!')
print('Attached to Node: {} | Current Balance: {}'.format(operator,w3.fromWei(w3.eth.getBalance(w3.eth.coinbase), 'ether')))

#Create databases and their tables
node_database.db.generate_mapping(create_tables=True)
#miner_database.db.generate_mapping(create_tables=True)


#Address of the contract
address = "0xec03f80715916439448dd463b57434101952e13c"
#Ip address of miner
ip_address = "192.168.0.27"
#Decrypted private key for pi's
private_key = "0xc388fcf3c141b1d5b9c73f804b56b55cd1fa4aa875b6d976a014feed1c53aade"


#Contract variables
#ABI of the contract
with open('../solidity/EnergyTrader.abi') as f:
    ABI = f.read().strip()



#Create contract instance to interact with
contract = w3.eth.contract(
    address=w3.toChecksumAddress(address),
    abi=ABI
)

#Global timer to ensure ERC's are accepted within a reasonable time
timer = None

#Nonce counter so multiple CTP's can be received at a time
nonce_counter = 1
#Ctp_counter to make sure tx nonce doesn't
ctp_counter = 1

#Keep track of this nodes trades. Will only mine ERC's received in order
erc_count = []
curr_erc = 0

possible_ids = list(range(1,101))

#Setup events to listen for calls from the smart contract

#Trade is sent when a ERC is received after a CTP. It signals to the miner to mine
#That corresponding transaction
trade = contract.eventFilter('trade',{'fromBlock': 0,'toBlock':'latest'})

#ctp is sent when a node signals to the smart contract it has a deal.
#node will sign a transaction and send it to the miner
ctp_accepted = contract.eventFilter('ctp_accepted',{'fromBlock': 0,'toBlock':'latest'})

#ctp deleted is sent whenever a ctp is removed from the contract without a trade.
#It signals other nodes that their id can be used and for the miner to remove 
#any corresponding signed tx's
ctp_deleted = contract.eventFilter('ctp_deleted',{'fromBlock': 0, 'toBlock': 'latest'})


#==========================#
# MAIN LOOP OF THE NETWORK #
#==========================#
def event_loop(poll_interval):
    #===================================================#
    # Handle Events and Python network db entries       #
    # 1. Handle each event for miner operator           #
    #       - Call event handler for each type of event #
    # 2. Handle each event for Node                     #
    #       - Call event handler for each type of event #
    # 3. Check node db for incoming ctp & erc           #
    #       - Check ctp's                               #
    #       - Check erc's                               #
    #===================================================#

    while True:

        if operator == "node":
            #Handle events from the smart contract
            #This checks for events coming from the smart contract.
            events = ctp_accepted.get_new_entries()
            for event in events:
                e = handle_event_node(event)
                if e is not None:
                    print(e)

            #Handle Trade Events
            events = trade.get_new_entries()
            for event in events:
                e = handle_event_node(event)
                if e is not None:
                    print(e)

            #Handle ctp_deleted events
            events = ctp_deleted.get_new_entries()
            for event in events:
                e = handle_event_node(event)
                if e is not None:
                    print(e)

          
            #3. Check node's db for ctp and erc
            #Check CTP's
            check_node_ctp()
            #CHECK ERC's
            check_node_erc()     

        #Sleep for 1 second before checking again
        time.sleep(poll_interval)

        

#=====================#
# Nodes event handler #
#=====================#
def handle_event_node(event):
    #===============================================#
    # Node Handling SC Events                       #
    # 1. Get event parameters                       #
    # 2. Handle ctp_accepted event                  #
    #       - create signed tx for amount in event  #
    #       - send signed tx to miner               #
    #       - reset timer time                      #
    # 3. Handle trade event                         #
    #       - increase ctp window                   #
    #       - stop ctp timer                        #
    # 4. Handle ctp_deleted event                   #
    #       - add ctp_id back into pool of ids      #
    #===============================================#
    global nonce_counter
    global ctp_counter
    global timer

    #1. Get parameters
    from_ad = event['args']['from']
    ctp_id = event['args']['ctp_id']
    to_ad = event['args']['to']
    amount = event['args']['amount']
    event_name = event['event']

    #2. Handle ctp_accepted event
    if event_name == "ctp_accepted":
        remove_used_id(ctp_id)
        #Check if the event is from this node
        if from_ad == w3.eth.coinbase:
            
            
            #Create transaction object
            nonce_temp = w3.eth.getTransactionCount(from_ad)+nonce_counter
            transaction = {
                'to': to_ad,
                'value': w3.toWei(amount, 'ether'),
                'gas': 900000,
                'gasPrice': 234567897654,
                #Nonce is set with nonce_counter to ensure it can be mined at a later date
                'nonce': nonce_temp
            }

            #Sign the transaction
            signed = w3.eth.account.signTransaction(transaction,private_key)

            print('###########################################')
            print('signed tx: {} with nonce: {} and amount: {}'.format(ctp_id,nonce_temp,amount))
            print('###########################################')

            #Peer sends raw transaction to miner
            #connect to server
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            
            server_address = (ip_address, 23456)
            #Send the signed raw transaction to miner
            sock.connect(server_address)
            data = signed.rawTransaction
            sock.sendall(data)
            time.sleep(1)
            sock.close()

            #Set timer for this ctp
            #timer.cancel()
            #imer = threading.Timer(30,timer_expire,[ctp_id])
            #timer.start()
            handle_erc_counter()
    #3. Handle trade event
    if event_name == "trade":
        #increase ctp_window
        if w3.eth.coinbase == from_ad:
            ctp_counter += 1
            #Stop timer for this id
            #timer.cancel()

    #4. Handle ctp_deleted event
    if event_name == "ctp_deleted":
        return_used_id(ctp_id)


#==================#
# Helper Functions #
#==================#

#Send ctp to smart contract
def send_ctp(new_id,amount,to):
    to = to.strip("\"")
    valid = w3.toChecksumAddress(to)
    transaction = {
            'from': w3.eth.coinbase,
            'gas': 900000
    }
    w3.personal.unlockAccount(w3.eth.coinbase,'test')
    contract.functions.acceptCTP(new_id,int(amount),valid).transact(transaction)

#Send erc to smart contract
def send_erc(new_id):
    transaction = {
            'from': w3.eth.coinbase,
            'gas': 900000
    }
    w3.personal.unlockAccount(w3.eth.coinbase,'test')
    contract.functions.receiveERC(int(new_id)).transact(transaction)

#Check nodes local db for new ctp's
def check_node_ctp():
    global curr_erc
    global ctp_counter
    global timer

    #If node is ready to accept new ctp
    if ctp_counter > 0:
        #Get next ctp
        ctp = node_database.get_ctps()
        if ctp is not None:
            #if we have enough money to handle ctp then accept it
            if w3.fromWei(w3.eth.getBalance(w3.eth.coinbase),"ether") >= ctp.amount:
                #Get new unique id
                new_id = get_new_id()
                print('#######################')
                print('RECEIVED CTP WITH ID: ',(new_id))
                print('######################')
                #Send it to smart contract
                send_ctp(new_id,ctp.amount,ctp.to_ad)
                #reduce window
                ctp_counter -= 1
                #insert id in waiting list
                erc_count.insert(0,new_id)
                #Delete CTP from local db
                node_database.delete_ctp(ctp.amount,ctp.from_ad,ctp.to_ad)
                #timer = threading.Timer(30,timer_expire,[new_id])
                #timer.start()
        #Update curr erc if needed
        #if curr_erc == 0:
            #handle_erc_counter()

#Check nodes local db for new erc's
def check_node_erc():
    global curr_erc

    erc = None
    if curr_erc != 0:
        erc = node_database.get_erc_id(curr_erc)
    if erc is not None:
        #Its the right id
        print('######################')
        print("RECEIVED ERC WITH ID: ",(erc.ctp_id))
        print('######################')
        send_erc(erc.ctp_id)
        handle_erc_counter()
        node_database.delete_erc(erc.ctp_id)

#Update node to accept next erc
def handle_erc_counter():
    global erc_count
    global curr_erc

    if len(erc_count) == 0:
        curr_erc = 0
    else:
        curr_erc = erc_count.pop()

#Pick a random id from available id's
def get_new_id():
    global possible_ids

    nid = random.choice(possible_ids)
    possible_ids.remove(nid)
    return nid

#Place id back into pool of available id's
def return_used_id(id):
    global possible_ids

    if id not in possible_ids:
        possible_ids.append(id)

#Remove id from pool of available id's
def remove_used_id(id):
    global possible_ids

    if id in possible_ids:
        possible_ids.remove(id)

#When a ctp's timer expires delete that ctp from contract
def timer_expire(id):
    global ctp_counter

    print('#########################')
    print('Timer expired for id: ',id)
    print('#########################')
    contract.functions.delete_ctp(id).transact({'from': w3.eth.coinbase})
    #increase ctp_window
    ctp_counter += 1
    #Update next erc
    handle_erc_counter()

#Check if the script has been run correctly.
if len(sys.argv) != 2:
    print('Ayylmao your using the script wrong. Try: python(3+) network.py (miner|node)')
else:
    event_loop(2)