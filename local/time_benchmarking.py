import node_database as nd
import time
import web3
import datetime
import random



#Connect to node
w3 = web3.Web3(web3.Web3.HTTPProvider('http://localhost:8885'))

#Connect to database
nd.db.generate_mapping(create_tables=True)

#Address variables
to_ad = "0x1147494773a0769c652ec0404a654f46022a5ad4"
from_ad = w3.eth.coinbase

#Amount to send
amount = 100

#Benchmark number
benchmark = 1

#Access the contract
#Contract variables
with open('../solidity/EnergyTrader.abi') as f:
    ABI = f.read().strip()

address = "0x487f9e81cebbcc311c0ca21c7c91eeb219f9bdbe"

#Create contract instance to interact with
contract = w3.eth.contract(
    address=w3.toChecksumAddress(address),
    abi=ABI
)

#Access the events from the smart contract
ctp = contract.eventFilter('ctp_accepted',{'fromBlock': 0,'toBlock':'latest'})
#deploy = contract.eventFilter('deploy',{'fromBlock': 0, 'toBlock': 'latest'})
trade = contract.eventFilter('trade',{'fromBlock': 0,'toBlock':'latest'})



def time_ctp(i):
    #send ctp
    valid = w3.toChecksumAddress(to_ad)
    transaction = {
        'from': w3.eth.coinbase,
        'gas': 900000
    }
    w3.personal.unlockAccount(w3.eth.coinbase,'test')
    #CTP timer
    startTime = datetime.datetime.now()
    contract.functions.acceptCTP(i,int(i),valid).transact(transaction)
    #wait for event
    print('waiting for event')
    while True:
        events = ctp.get_new_entries()
        for event in events:
            print('event found')
            ctpTime = datetime.datetime.now() - startTime
            return ctpTime

def time_erc(i):
    #send ctp
    valid = w3.toChecksumAddress(to_ad)
    transaction = {
        'from': w3.eth.coinbase,
        'gas': 900000
    }
    w3.personal.unlockAccount(w3.eth.coinbase,'test')
    #CTP timer
    startTime = datetime.datetime.now()
    contract.functions.receiveERC(i).transact(transaction)
    #wait for event
    print('waiting for event')
    while True:
        events = trade.get_new_entries()
        for event in events:
            print('event found')
            ercTime = datetime.datetime.now() - startTime
            return ercTime
i = 0
ctpTimes = []
ercTimes = []
ctpAverage = 0
ercAverage = 0
while i < 10:
    time.sleep(1)
    ctpTimes.append(time_ctp(i))
    ercTimes.append(time_erc(i))
    i += 1

print('CTP times:')
for t in ctpTimes:
    t = t.total_seconds()
    print('ctp took: ',t)
    ctpAverage += t

print('average: ',ctpAverage/10)


print('ERC times:')
for t in ercTimes:
    t = t.total_seconds()
    print('erc took: ',t)
    ercAverage += t

print('average: ',ercAverage/10)
