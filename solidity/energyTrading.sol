pragma solidity ^0.4.8;


contract EnergyTrader {

    // Contract variables: 
    /* Allow owner to make certain changes */
    address owner;
    /* Helper string to debug contract operations */
    string lastTransaction;

    /* Pending transactions database */
    mapping(address => mapping(uint => bool)) private exists;
    mapping(address => mapping(uint => address)) private pending_to;
    mapping(address => mapping(uint => uint)) private amounts;

    //Set owner
    constructor() public {
        owner = msg.sender;
        lastTransaction = "New Deploy Success. Waiting on next command";
    }

    /* Trade event: Sent when a ERC is recieved with a corresponding CTP 
     * in the CTP pending transactions table 
     */
    event trade(
        address to,
        address from,
        uint256 amount,
        uint256 ctp_id
    );

    /* CTP event: Sent when a CTP is recieved  
     */    
     event ctp_accepted(
        address to,
        address from,
        uint256 amount,
        uint256 ctp_id
    );
    /* Delete event: Sent when a ctp id is removed from the pending database
     */ 
    event ctp_deleted(
        address to,
        address from,
        uint256 amount,
        uint256 ctp_id
    );

    //Allows nodes to send CTP's to the contract
    function acceptCTP (uint256 id, uint256 amount, address to) public returns (string memory){
        //Check if id is already in use
        bool check = exists[msg.sender][id];
        if (check == false) {
            //Not in use
            exists[msg.sender][id] = true;
            pending_to[msg.sender][id] = to;
            amounts[msg.sender][id] = amount;

            emit ctp_accepted(to,msg.sender,amount,id);
            lastTransaction = "Creating New CTP";
        }
        if (check == true) {
            //The id is in use, need to send back error.
            lastTransaction = "CTP_ID already exists in contract!";
        } 

        return "Done in CTP";
    }
    
    //Allows nodes to send CTP's to the contract
    function receiveERC (uint256 id) public {
        //Check if address is already mapped
        bool check = exists[msg.sender][id];
        if (check == true) {

            address to = pending_to[msg.sender][id];
            uint amount = amounts[msg.sender][id];
            
            exists[msg.sender][id] = false;
            emit trade(to,msg.sender,amount,id);
            lastTransaction = "Removed old CTP";

        } else {
            lastTransaction = "Error, this id already exists";
        }
    }

    function delete_ctp (uint id) public {
        exists[msg.sender][id] = false;
        address to_ad = pending_to[msg.sender][id];
        uint amount = amounts[msg.sender][id];

        emit ctp_deleted(to_ad,msg.sender,amount,id);
        lastTransaction = "Deleted ctp";
    }


    
    function CLT () view public returns (string memory) {
        return lastTransaction;
    }
}