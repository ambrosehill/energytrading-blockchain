Smart Contract
==============

The smart contract is the bottle neck of the network and in an attempt to optimise the network speed functionality was removed from the smart contract.

# Features
Currently the smart contract only has 3 features:

+ Accept a commit to pay message from a participant. This CTP is stored on the smart contracts local storage
+ Accept an energy receipt confirmation. If the smart contract currently has a corresponding CTP (using the id and from address hash) this ERC is accepted.
+ Receive a delete commit to pay. When a transaction fails for any reason and needs to be rolled back this function is executed and the CTP is removed from the smart contract

  Each of these features releases an event containing the id, from address, to address and amount that allows other nodes to react to each event. These events are what
  dictates the command network and controls the autonmous ether transfer.